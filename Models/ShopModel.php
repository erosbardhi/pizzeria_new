<?php

namespace Models;

class ShopModel extends Model {
    /**
     * Undocumented function
     *
     * @return array
     */
    public function shopping(): array
    {
        return $this->db->exec('SELECT * FROM products');
    }
    /**
     * DB SAVE ALL ORDERS
     * 
     * Undocumented function
     *
     * @param  $order_name
     * @param  $order_description
     * @param  $order_info
     * @param  $order_price
     * @param  $order_quantity
     * @return boolean
     */
    public function saveOrders ($order_name, $order_description, $order_info, $order_price, $order_quantity, $order_total_price) : bool {

        $isStored = $this->db->exec('INSERT INTO orders (order_name, order_description, order_info, order_price, order_quantity ) VALUES(?, ?, ?, ?, ? )', 
        [$order_name, $order_description, $order_info, $order_price, $order_quantity  ]);
        return $isStored;
    }
    /**
     * ADMIN  ALL  PAYED ORDERS
     * 
     * Undocumented function
     *
     * @return array
     */
    public function showOrders(): array
    {
        return $this->db->exec('SELECT * FROM orders');
    }

}

