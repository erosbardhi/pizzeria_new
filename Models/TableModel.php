<?php

namespace Models;

class TableModel extends Model {

    public function showtable(): array
    {
        return $this->db->exec('SELECT * FROM reservation');
    }

    public function deleteTable(int $id) {
        $isDeleted = $this->db->exec('DELETE FROM reservation WHERE reservation_id=?', $id);
        return $isDeleted;
         
         }
    
     
}