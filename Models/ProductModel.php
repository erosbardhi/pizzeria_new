<?php

namespace Models;

class ProductModel extends Model {

    //// ADMIN PRODUCTS SIDE
    public function showproduct(): array
    {
        return $this->db->exec('SELECT * FROM products');
    }

    public function product($id) {
        return $this->db->exec('SELECT * FROM products WHERE product_id = ?', [$id]);
    }

    public function addProduct (string $product_name, string $product_descpription, string $product_info, string $product_price) : bool {
        $isStored = $this->db->exec('INSERT INTO products (product_name, product_description, product_info, product_price) VALUES(?, ?, ?, ?)', [$product_name, $product_descpription, $product_info, $product_price]);
        return $isStored;
    }

    public function editProduct (string $product_name, string $product_description, string  $product_info, string $product_price,  int $id) : bool {
        $isStored = $this->db->exec('UPDATE products SET product_name = ?, product_description = ?, product_info = ?, product_price=? WHERE product_id = ?', [$product_name, $product_description, $product_info, $product_price, $id]);
        return $isStored;
     }
    
     public function deleteProduct(int $id) {
    $isDeleted = $this->db->exec('DELETE FROM products WHERE product_id=?', $id);
    return $isDeleted;
     
     }

}
