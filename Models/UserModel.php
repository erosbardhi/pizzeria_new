<?php
namespace Models;

class UserModel extends Model {
    /**
     * Alle User ermitteln
     * Get All Users
     * @return array
     */
    public function users() : array {
        return $this->db->exec('SELECT * FROM users');
    }

    /**
     * Einen einzelnen User ermitteln
     *
     * @param integer $id
     * @return array
     */
    public function user(int $id) : array {
        $user = $this->db->exec('SELECT * FROM users WHERE id=?', $id);

        if (count($user) === 0) {
            return [];
        }

        return $user[0];
    }

     /**
     * Returns 0 if user data doesn´t exist and 1 if exists/is correct 
     *
     * @param string $userEmail
     * @param string $userPassword
     * 
     */
    public function getLoggedInUsers($user_email,$password)
    {
        $getLoggedInUsers = $this->db->exec('SELECT * FROM users WHERE user_email=? AND password=?', [$user_email, $password]);
        if ($this->db->count() == 0)
            return false;

        return $getLoggedInUsers;
    }
    /**
     * 
     * 
     * 
     */
    
    public function addUser ($email, $pw, $name, $lname, $adrs, $pnumber ) : bool {
        
        $isStored = $this->db->exec('INSERT INTO users (user_email, password, name, last_name, address, user_phone) VALUES(?, ?, ?, ?, ?, ?)', 
        [$email, $pw, $name, $lname, $adrs, $pnumber]);
        return $isStored;
    } 
    
    
    
    public function resTable ($res_name, $res_email, $res_phone, $res_date, $res_time, $res_message) : bool {
        
        $isStored = $this->db->exec('INSERT INTO reservation (res_name, res_email, res_phone, res_date, res_time, res_message) VALUES(?, ?, ?, ?, ?, ?)', 
        [$res_name, $res_email, $res_phone, $res_date, $res_time, $res_message]);
        return $isStored;
    }


    public function msgContact ($msg_name, $msg_email, $msg_text) : bool {
        
        $isStored = $this->db->exec('INSERT INTO messages (msg_fullname, msg_email, msg_text) VALUES(?, ?, ?)', 
        [$msg_name, $msg_email, $msg_text]);
        return $isStored;
    }

    public function showcontact(): array
    {
        return $this->db->exec('SELECT * FROM messages');
    }

    public function deleteContact(int $id) {
        $isDeleted = $this->db->exec('DELETE FROM messages WHERE msg_id=?', $id);
        return $isDeleted;
         
         }

}