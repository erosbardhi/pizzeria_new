<?php
namespace Controller;
use \Template;

class RegisterController {
    public function register($f3, $params) {

        
        if (!empty($_POST)) {
            $gump = new \GUMP('de');

            $gump->validation_rules(array(
                'email'    => 'required',
                'password'    => 'required|alpha_numeric|max_len,255|min_len,8',
                /* 'firstname'    => 'required|max_len,255',
                'lastname'    => 'required|max_len,255',
                'phonenumber'    => 'required',
                'address'    => 'required' */
            ));

            $validData = $gump->run($_POST);

            if ($validData === false) {
                $errors = $gump->get_errors_array();
                $f3->set('errors', $errors);
                $f3->set('values', $_POST);
            }
            else {
                $um = new \Models\UserModel();
                $isStored = $um->addUser($validData['email'], $validData['password'], $validData['name'], $validData['last_name'], $validData['address'], $validData['user_phone'] );
                if ($isStored === true) {
                    $f3->set('alertSuccess', 'Thank You for your Registration!');
                }
                else {
                    $f3->set('alertError', 'Warning! You could not add the new User.');
                }
            }
        }


        $f3->set('pageTitle', 'Register Form');
        $f3->set('mainHeading', '');
        $f3->set('content', '/views/content/register.html');

        
        echo Template::instance()->render('/views/index.html');
    }
}