<?php
namespace Controller;
use \Template;

class PagesController {
    public function table($f3, $params) {
        
        $f3->set('pageTitle', 'Reserve a Table');
        $f3->set('mainHeading', '');
        $f3->set('content', '/views/content/table-book.html');

        if (!empty($_POST)) {
            $gump = new \GUMP('de');

            $gump->validation_rules(array(
                'res_name'    => 'required',
                'res_message'    => 'required',
                'res_email'    => 'required',
                'res_phone'    => 'required'
                
                
            ));

            $validData = $gump->run($_POST);

            if ($validData === false) {
                $errors = $gump->get_errors_array();
                $f3->set('errors', $errors);
                $f3->set('values', $_POST);
            }
            else {
                $um = new \Models\UserModel();
                $isStored = $um->resTable($validData['res_name'],$validData['res_email'], $validData['res_phone'], $validData['res_date'], $validData['res_time'], $validData['res_message'] );
                if ($isStored === true) {
                    $f3->set('alertSuccess', 'Table Reserved, Thank You! We will send you a Message with in 24 Hours');
                }
                else {
                    $f3->set('alertError', 'Warning! You could not Reserve the Table.');
                }
            }
        }

        
        echo Template::instance()->render('/views/index.html');
    }

    public function menu($f3, $params) {

        
        $f3->set('pageTitle', 'Menu');
        $f3->set('mainHeading', '');
        $f3->set('content', '/views/content/menu.html');

        
        echo Template::instance()->render('/views/index.html');
    }

    public function buffet($f3, $params) {

        
        $f3->set('pageTitle', 'Mittagsbuffet');
        $f3->set('mainHeading', '');
        $f3->set('content', '/views/content/buffet.html');

        
        echo Template::instance()->render('/views/index.html');
    }

    public function contact($f3, $params) {

        if (!empty($_POST)) {
            $gump = new \GUMP('de');

            $gump->validation_rules(array(
                'msg_fullname'    => 'required',
                'msg_email'    => 'required',
                'msg_text'    => 'required'
            ));

            $validData = $gump->run($_POST);

            if ($validData === false) {
                $errors = $gump->get_errors_array();
                $f3->set('errors', $errors);
                $f3->set('values', $_POST);
            }
            else {
                $um = new \Models\UserModel();
                $isStored = $um->msgContact($validData['msg_fullname'], $validData['msg_email'], $validData['msg_text'] );
                if ($isStored === true) {
                    $f3->set('alertSuccess', '');
                }
                else {
                    $f3->set('alertError', 'Warning! You could not send this Message.');
                }
            }
        }

        
        $f3->set('pageTitle', 'Contact');
        $f3->set('mainHeading', '');
        $f3->set('content', '/views/content/contact.html');

    

        
        echo Template::instance()->render('/views/index.html');
    }

    public function showcontact($f3, $params) {       ////////////////////////////////////////////////

        $pm = new \Models\UserModel();
        $contacts = $pm->showcontact();
        $f3->set('messages', $contacts);
       

        $f3->set('jScripts', ['/js/student.js']);


        $f3->set('pageTitle', 'All Messages');
        $f3->set('mainHeading', '');
        $f3->set('content', '/views/content/contact-all.html');

       
        echo Template::instance()->render('/views/index.html');
    }

    public function deleteContact($f3, $params)
    {
        $cid = $params['cid'];
        if (!filter_var($cid, FILTER_VALIDATE_INT)) {
            echo 'Error: Can not Delete';
        } else {
            $sm = new \Models\UserModel();
            if ($sm->deleteContact($cid)) {
                echo 'Message_deleted';
            } else {
                echo 'Error';
            }
        }
    }

    public function about($f3, $params) {

        
        $f3->set('pageTitle', 'About Us');
        $f3->set('mainHeading', '');
        $f3->set('content', '/views/content/about.html');

        
        echo Template::instance()->render('/views/index.html');
    }

   

    public function gallery($f3, $params) {

        
        $f3->set('pageTitle', 'Gallery');
        $f3->set('mainHeading', '');
        $f3->set('content', '/views/content/gallery.html');

        
        echo Template::instance()->render('/views/index.html');
    }

    public function birthday($f3, $params) {

        
        $f3->set('pageTitle', 'Kinderparty');
        $f3->set('mainHeading', '');
        $f3->set('content', '/views/content/birthday.html');

        
        echo Template::instance()->render('/views/index.html');
    }

    public function catering($f3, $params) {

        
        $f3->set('pageTitle', 'Catering');
        $f3->set('mainHeading', '');
        $f3->set('content', '/views/content/catering.html');

        
        echo Template::instance()->render('/views/index.html');
    }

    public function allUsers($f3, $params) {

        $pm = new \Models\UserModel();
        $users = $pm->users();
        $f3->set('users', $users);
        
        $f3->set('pageTitle', 'Users');
        $f3->set('mainHeading', '');
        $f3->set('content', '/views/content/all-users.html');

        
        echo Template::instance()->render('/views/index.html');
    }

    public function loggedin($f3, $params) {  /////////  Thank you for Shopping //////////


        $f3->set('pageTitle', 'loggedIn');
        $f3->set('mainHeading', '');
        $f3->set('content', '/views/content/userLoggedIn.html');

       
        echo Template::instance()->render('/views/index.html');
    }
    
}















 /* public function pizzakeller($f3, $params) {
        
        $f3->set('pageTitle', 'Pizza Keller Home');
        $f3->set('mainHeading', '');
        $f3->set('content', '/views/content/pizzakeller.html');

        
        echo Template::instance()->render('/views/index.html');
    }
    public function rossini($f3, $params) {
        
        $f3->set('pageTitle', 'Rossini Home');
        $f3->set('mainHeading', '');
        $f3->set('content', '/views/content/rossini.html');

        
        echo Template::instance()->render('/views/index.html');
    }
    public function caramia1($f3, $params) {
        
        $f3->set('pageTitle', 'Cara Mia 1 Home');
        $f3->set('mainHeading', '');
        $f3->set('content', '/views/content/caramia1.html');

        
        echo Template::instance()->render('/views/index.html');
    }
    public function caramia2($f3, $params) {
        
        $f3->set('pageTitle', 'Cara Mia 2 Home');
        $f3->set('mainHeading', '');
        $f3->set('content', '/views/content/caramia2.html');

        
        echo Template::instance()->render('/views/index.html');
    }
    public function botendienst($f3, $params) {
        
        $f3->set('pageTitle', 'Botendienst Home');
        $f3->set('mainHeading', '');
        $f3->set('content', '/views/content/botendienst.html');

        
        echo Template::instance()->render('/views/index.html');
    } */