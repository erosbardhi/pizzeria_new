<?php
namespace Controller;
use \Template;

class ShopController {           ///////   User Shop Side   /////

   
    public function shopOnline($f3, $params) {  /////////  USER ONLINE SHOP ALL PRODUCTS//////////

        $pm = new \Models\ShopModel();
        $products = $pm->shopping();
        $f3->set('products', $products);


        $f3->set('pageTitle', 'Alle Produkte');
        $f3->set('mainHeading', '');
        $f3->set('content', '/views/content/shopping.html');

       
        echo Template::instance()->render('/views/index.html');
    }

    
    public function showshop($f3, $params) { ///////////////  Admin Show Orders ////////////////////////
        $sm = new \Models\ShopModel();

        if (!empty($_POST)) {
            $gump = new \GUMP('de');

            $gump->validation_rules(array(
                ''    => '',
                ''    => '',
                ''    => ''
            ));

            $validData = $gump->run($_POST);

            if ($validData === false) {
                $errors = $gump->get_errors_array();
                $f3->set('errors', $errors);
                $f3->set('values', $_POST);
            }
            else {
                $isStored = $sm->saveOrders($validData['order_name'], $validData['order_description'], $validData['order_info'], $validData['order_price'], $validData['order_quantity'], $validData['order_total_price'] );
                if ($isStored === true) {
                    $f3->set('alertSuccess', '');
                }
                else {
                    $f3->set('alertError', 'Warning!');
                }
            }
        }

        $orders = $sm->showOrders();
        
        $f3->set('orders', $orders);
        $f3->set('pageTitle', 'All Orders');
        $f3->set('mainHeading', '');
        $f3->set('content', '/views/content/shop-admin.html');

       
        echo Template::instance()->render('/views/index.html');

    }

   

    public function saveCart($f3, $params) {  /////////  Save Orders //////////
        $um = new \Models\ShopModel();

               //var_dump( json_decode($f3->BODY) );
            
         $cart = json_decode($f3->BODY); 
        //var_dump($cart->cart);
        $orderQuantity = count($cart->cart);
        $orderPrice = 0;
        foreach($cart->cart as $order){
            $orderPrice = $orderPrice + ($order->quantity * floatval($order->price));    
        }
        //var_dump($orderPrice);
        $isStored = $um->saveOrders("testname", "testdescription", "info", $orderPrice,$orderQuantity, $orderPrice );
        if ($isStored === true) {
            $f3->set('alertSuccess', 'Thank you');
        }
        else {
            $f3->set('alertError', 'Warning! You can not order this Product.');
        }
       

         /*  echo "Thank you for your Order...Buon Appetit"; */ 

    }


    public function thankyou($f3, $params) {  /////////  Thank you for Shopping //////////


        $f3->set('pageTitle', 'Cart');
        $f3->set('mainHeading', '');
        $f3->set('content', '/views/content/thankyou.html');

       
        echo Template::instance()->render('/views/index.html');
    }
     
}