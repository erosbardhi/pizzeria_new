<?php
namespace Controller;
use \Template;


class IndexController extends \App\LogChecker {


    public function index($f3, $params) {
        
        $f3->set('pageTitle', 'Home');
        $f3->set('mainHeading', '');
        $f3->set('content', '/views/content/home.html');


        echo Template::instance()->render('/views/index.html');
    }

}


 