<?php
namespace Controller;
use \Template;

class ProductController {
    
    public function product($f3, $params) {       ////////////////////// ADMIN First Page/////////////////////////////

        $pm = new \Models\ProductModel();
        $products = $pm->showproduct();
        $f3->set('products', $products);
       


        $f3->set('pageTitle', 'Admin Page');
        $f3->set('mainHeading', '');
        $f3->set('content', '/views/content/products.html');

        
        echo Template::instance()->render('/views/index.html');
    }
   
    public function showproduct($f3, $params) {       /////////////////////// ADMIN show all Products/////////////////////////

        $pm = new \Models\ProductModel();
        $products = $pm->showproduct();
        $f3->set('products', $products);
       

        $f3->set('jScripts', ['/js/student.js']);


        $f3->set('pageTitle', 'Alle Produkte');
        $f3->set('mainHeading', '');
        $f3->set('content', '/views/content/products-all.html');

       
        echo Template::instance()->render('/views/index.html');
    }
    
    public function add($f3, $params)            ///////////////////  ADMIN Add new Product////////////////////
    {
        if (!empty($_POST)) {
            $gump = new \GUMP('de');

            $gump->validation_rules(array(
                'product_name'    => 'required',
                'product_description'    => 'required',
                'product_info'    => 'required',
                'product_price'    => 'required'
            ));

            $validData = $gump->run($_POST);

            if ($validData === false) {
                $errors = $gump->get_errors_array();
                $f3->set('errors', $errors);
                $f3->set('values', $_POST);
            }
            else {
                $um = new \Models\ProductModel();
                $isStored = $um->addProduct($validData['product_name'], $validData['product_description'], $validData['product_info'], $validData['product_price']);
                if ($isStored === true) {
                    $f3->set('alertSuccess', 'Neues Produkt erfolgreich eingetragen!');
                }
                else {
                    $f3->set('alertError', 'Fehler! Das neue Produkt konnte nicht eingetragen werden.');
                }
            }
        }

        $f3->set('pageTitle', 'Add New Product');
        $f3->set('mainHeading', '');
        $f3->set('content', '/views/content/product-add.html');

        // Template ausgeben
        echo Template::instance()->render('/views/index.html');

    
    }

    public function edit($f3, $params)       /////////////////////  ADMIN Edit Product///////////////////////////////////
    {
        $pid = $params['pid'];
        
        if (!filter_var($pid, FILTER_VALIDATE_INT)) {
            $values = [];
        } else {
            $um = new \Models\ProductModel();
            $values = $um->product($pid);
        }

       
        if (!empty($_POST)) {
            $gump = new \GUMP('de');

            $gump->validation_rules(array(
                'product_name'    => 'required',
                'product_description'    => 'required',
                'product_info'    => 'required',
                'product_price'    => 'required'
            ));

            $validData = $gump->run($_POST);

            if ($validData === false) {
                $errors = $gump->get_errors_array();
                $f3->set('errors', $errors);
                $values[0] = $_POST;
            }
            else {
                $um = new \Models\ProductModel();
                $isStored = $um->editProduct($validData['product_name'], $validData['product_description'], $validData['product_info'], $validData['product_price'], $pid);
                $values[0] = $validData;
    
                if ($isStored === true) {
                    $f3->set('alertSuccess', 'Product Edited!');
                }
                else {
                    $f3->set('alertError', 'Warning! You can not edit this Product');
                }
            }
        }
        
        $f3->set('values', $values[0]);

        $f3->set('pageTitle', 'Edit Product');
        $f3->set('mainHeading', '');
        $f3->set('content', '/views/content/product-edit.html');

       
        echo Template::instance()->render('/views/index.html');
    }

    public function delete($f3, $params)   ///////////////// ADMIN Delete Product////////////
    {
        $pid = $params['pid'];
        if (!filter_var($pid, FILTER_VALIDATE_INT)) {
            echo 'Error: Can not Delete';
        } else {
            $sm = new \Models\ProductModel();
            if ($sm->deleteProduct($pid)) {
                echo 'Product_deleted';
            } else {
                echo 'Error';
            }
        }
    }
     
}

 