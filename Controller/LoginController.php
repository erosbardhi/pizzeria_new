<?php
namespace Controller;

use \Template;
use \App\SystemHelper as SH; 

class LoginController extends \App\LogChecker{

    public function beforeroute() {

    }

    public function login($f3, $params) {

    

        $f3->set('pageTitle', 'Login');
        
        $f3->set('content', '/views/content/login.html');

        
        echo Template::instance()->render('/views/index.html');
    } 


    function authenticate($f3, $params){

        if (isset($_POST['user_email']) && isset($_POST['password'])) {
            
            $gump = new \GUMP('en');

            // gump validates form entries upon these rules
            $gump->validation_rules(array(
                'user_email' => 'required',
                'password' => 'required',

            ));

            // further Form entry sanitation
            $gump->filter_rules(array(
                'user_email' => 'trim|sanitize_string',
                'password' => 'trim',
            ));

             // initialize gump
             $validLoginData = $gump->run($_POST);

             // getting email and password from sent POST
             $user_email = $validLoginData['user_email'];
             $password = $validLoginData['password'];

             // Connect to server and select databse.
            $user = new \Models\UserModel($this->db);
            $result = $user->getLoggedInUsers($user_email,$password);
            if ($result !== false) {

    
                SH::customSessionStore('user_id', $result);
                $f3->reroute('/loggedin');
            } else {
                
                $f3->reroute('/login');
                $f3->set('content', '/views/content/login.html');

                echo Template::instance()->render('/views/index.html');
            }
        }
        
    }
    
    public function logout($f3, $params)
    {
        // $this->customSessionDestroy();
        SH::customSessionDestroy();
        $f3->reroute('/');
    } 
}