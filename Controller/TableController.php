<?php
namespace Controller;
use \Template;

class TableController {
    public function showtable($f3, $params) {
        
        $f3->set('pageTitle', 'Reservierungen');
        $f3->set('mainHeading', '');
        $f3->set('content', '/views/content/table-all.html');


        $pm = new \Models\TableModel();
        $reservation = $pm->showtable();
        $f3->set('reservation', $reservation);
       

       /*  $f3->set('jScripts', ['/js/student.js']); */

        echo Template::instance()->render('/views/index.html');
    }

    public function delete($f3, $params)
    {
        $tid = $params['tid'];
        if (!filter_var($tid, FILTER_VALIDATE_INT)) {
            echo 'Error: Can not Delete';
        } else {
            $sm = new \Models\TableModel();
            if ($sm->deleteTable($tid)) {
                echo 'Reservation_deleted';
            } else {
                echo 'Error';
            }
        }
    }

}