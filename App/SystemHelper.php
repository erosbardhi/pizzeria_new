<?php

namespace App;

class SystemHelper
{
    
    /**
     * if session_id is empty string, then session_id has not been initialized
     * then set session_id named 'session1'
     * start session needs 2 parameters, name and value
     * always close session after writing
     */
    public static function customSessionStore($name, $value)
    {

        if (session_id() == '') {
            session_id('session1');
        }
        session_start();
        $_SESSION[$name] = $value;
        // self::$_SESSION[$name] = $value;
        session_write_close();
    }
    
    /** 
     * Read current active session
    */
    public static function customSessionRead($name)
    {

        if (session_id() == '') {
            session_id('session1');
        }
        session_start();
        session_write_close();

        return $_SESSION[$name];
    }
    
    
    /**
     * gets called when user clicks logout button
     */
    public static function customSessionDestroy()
    {
        session_start();
        session_destroy();
    }
}
