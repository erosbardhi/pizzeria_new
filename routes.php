<?php
  //////////////////// index.html
$f3->route('GET /', 'Controller\IndexController->index');
$f3->route('POST /', 'Controller\IndexController->index');


//////////////////// Login and Register
$f3->route('GET /login', 'Controller\LoginController->login'); 
$f3->route('POST /authenticate', 'Controller\LoginController->authenticate');
$f3->route('GET /register', 'Controller\RegisterController->register');           
$f3->route('POST /register', 'Controller\RegisterController->register');           

////////////////////// Admin Crud
$f3->route('GET /products', 'Controller\ProductController->product');
$f3->route('POST /products', 'Controller\ProductController->product');
$f3->route('GET /products-all', 'Controller\ProductController->showproduct'); 
$f3->route('GET /product-add', 'Controller\ProductController->add');
$f3->route('POST /product-add', 'Controller\ProductController->add');
$f3->route('GET /products-all/@pid/edit', 'Controller\ProductController->edit');    
$f3->route('POST /products-all/@pid/edit', 'Controller\ProductController->edit');   
$f3->route('GET /products-all/@pid/delete', 'Controller\ProductController->delete');
$f3->route('GET /table-all', 'Controller\TableController->showtable');   
$f3->route('GET /table-all/@tid/delete', 'Controller\TableController->delete');
$f3->route('GET /contact-all', 'Controller\PagesController->showcontact'); 
$f3->route('GET /contact-all/@cid/delete', 'Controller\PagesController->deleteContact');
$f3->route('GET /all-users', 'Controller\PagesController->allUsers');   
$f3->route('GET /all-users/@uid/delete', 'Controller\PagesController->delete');
$f3->route('GET /shop-admin', 'Controller\ShopController->showshop');   

///////////////////////// CONTACT
$f3->route('GET /contact', 'Controller\PagesController->contact');
$f3->route('POST /contact', 'Controller\PagesController->contact');

////////////////////  USER Online Shop
$f3->route('GET /shopping', 'Controller\ShopController->shopOnline');
$f3->route('POST /shopping', 'Controller\ShopController->shopOnline'); 
$f3->route('GET /cart', 'Controller\ShopController->saveCart');
$f3->route('POST /cart', 'Controller\ShopController->saveCart');
$f3->route('GET /thankyou', 'Controller\ShopController->thankyou');
$f3->route('POST /thankyou', 'Controller\ShopController->thankyou');
$f3->route('GET /loggedin', 'Controller\PagesController->loggedin');
$f3->route('POST /loggedin', 'Controller\PagesController->loggedin');

////////////////////// Tisch Reservierung
$f3->route('GET /table', 'Controller\PagesController->table');                    
$f3->route('POST /table', 'Controller\PagesController->table');              


//////////////////////// Pages
$f3->route('GET /menu', 'Controller\PagesController->menu');
$f3->route('GET /buffet', 'Controller\PagesController->buffet');
$f3->route('GET /about', 'Controller\PagesController->about');
$f3->route('GET /gallery', 'Controller\PagesController->gallery');
$f3->route('GET /birthday', 'Controller\PagesController->birthday');
$f3->route('GET /catering', 'Controller\PagesController->catering');


////////////////////////Learning and testing
$f3->route('GET /learning', 'Controller\ALearning->learning');















/* $f3->route('GET /pizzakeller', 'Controller\PagesController->pizzakeller');
$f3->route('GET /rossini', 'Controller\PagesController->rossini');
$f3->route('GET /caramia1', 'Controller\PagesController->caramia1');
$f3->route('GET /caramia2', 'Controller\PagesController->caramia2');
$f3->route('GET /botendienst', 'Controller\PagesController->botendienst');  */


































/* $f3->route('GET /test', function(){
    echo 'Hello World';
}); */

