<head>
    <!-- <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title><?= ($pageTitle) ?></title> -->
    <!-- <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bulma@0.8.0/css/bulma.min.css"> -->
    <link rel="stylesheet" href="css/products.css">
</head>
  <section class="hero is-primary">
      <div class="hero-body">
        <div class="container">
            <div class="field">
                <p class="control has-icons-left">
                  <span class="select">
                    <select>
                      <option>Wähle deine Filiale</option>
                      <option>Pizza Keller</option>
                      <option>Pizzeria Rossinis</option>
                      <option>Cara Mia 1</option>
                      <option>Cara Mia 2</option>
                      <option>Botendienst</option>
                    </select>
                  </span>
                  <span class="icon is-small is-left">
                    <i class="fas fa-globe"></i>
                  </span>
                </p>
              </div>
        </div>
      </div>
    </section>
    
    <div class="demo">
      <table class="table is-responsive">
        <thead>
          <tr>
            <th>Product ID</th>
            <th>Product Name</th>
            <th>Product Descpription</th>
            <th>Product Info</th>
            <th>Product Price</th>
          </tr>
        </thead>
        <tbody>
          <!-- Das ist ein Foreach von Fatfree (repeat) -->
            <?php foreach (($products?:[]) as $row): ?>
                <tr>
                    <td><?= ($row['product_id']) ?></td>
                    <td><?= ($row['product_name']) ?></td>
                    <td><?= ($row['product_description']) ?></td>
                    <td><?= ($row['product_info']) ?></td>
                    <td><?= ($row['product_price']) ?></td>  
                <td><a href="/product/<?= ($row['id']) ?>/edit" class="button is-primary">Edit</a></td>
                <td><button class="button is-danger" data-url="/user/<?= ($row['id']) ?>/delete">Delete</button></td>
                </tr>
    
            <?php endforeach; ?>  
        </tbody>
      </table>
    
    </div>

        
