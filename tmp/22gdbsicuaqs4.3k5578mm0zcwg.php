<?php echo $this->render('/views/modules/alert.html',NULL,get_defined_vars(),0); ?>

<form action="" method="POST">
        <label for="product_name" class="label">Product Name</label>
        <?php if ($errors['product_name']): ?>
            <div class="field-error"><?= ($errors['product_name']) ?></div>
        <?php endif; ?>
        <input class="input is-success" type="text" name="product_name" id="product_name" value="<?= ($values['product_name']) ?>">
        
        
        
        
        <label for="product_description" class="label">Product Description</label>
        <?php if ($errors['product_description']): ?>
            <div class="field-error"><?= ($errors['product_description']) ?></div>
        <?php endif; ?>
        <input class="input is-success" type="text" name="product_description" id="product_description" value="<?= ($values['product_description']) ?>">
        
            
        
        <label for="product_info" class="label">Product Info</label>
        <?php if ($errors['product_info']): ?>
            <div class="field-error"><?= ($errors['product_info']) ?></div>
        <?php endif; ?>
        <input class="input is-success" type="text" name="product_info" id="product_info" value="<?= ($values['product_info']) ?>">

        
        
        <label for="product_price" class="label">Product Price</label>
        <?php if ($errors['product_price']): ?>
            <div class="field-error"><?= ($errors['product_price']) ?></div>
        <?php endif; ?>
        <input class="input is-success" type="text" name="product_price" id="product_price" value="<?= ($values['product_price']) ?>">
        
        <br>
        <br>
        
            <div class="control">
                <button class="button is-primary">Submit</button>
            </div>
        

</form>