<!DOCTYPE html>
<html lang="en" class="no-js">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="Farina is modern and responsive bakery, cakery or restaurant template.">
	

	<!--===  FONTS ===-->
	<link href="https://fonts.googleapis.com/css?family=Lato:300,300i,400,400i,700|Playfair+Display:400,400i,700,900" rel="stylesheet">

	<!--=== STYLES ===-->
	<link rel="stylesheet" href="assets/css/vendors/bootstrap.min.css">
	<link rel="stylesheet" href="assets/css/vendors/slick.css">
	<link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
	<link rel="stylesheet" href="assets/css/style.css">
</head>
<body>
	

	<!--=== Hero ===-->
	<div class="hero u-bg-p-bottom" style="background-image: url('/assets/img/home/sunset-field-of-grain-5980.jpg');">
		<div class="container">
			<div class="row">
				<div class="col-lg-8 offset-lg-2 text-center">
					<h1 class="hero__title u-font-serif" data-aos="zoom-in">Freshly baked everyday</h1>
					<p class="hero__subtitle mb-5" data-aos="zoom-in" data-aos-delay="100">Die erste Holzofen-Pizzeria im Norden Wiens (Eröffnung 2. Oktober 1982) und eine der ersten Pizzerien der Stadt.</p>
					<a href="#menu" class="btn js-on-page-nav" data-aos="zoom-in" data-aos-delay="100">Check Menu</a>
				</div>
			</div>
		</div>
	</div>
	<!--=== end of Hero ===-->

	<!--=== Story ===-->
	<div class="mt-7 mb-7">
		<div class="container">
			<div class="row align-items-center">
				<div class="col-md-6 d-none d-md-block">
					<img src="/assets/img/home/woman-pizza.jpg" data-aos="zoom-in-right" />
				</div>
				<div class="col-md-6">
					<div class="story-block pl-md-4" data-aos="zoom-in-left">
						<p class="story-block__upper-title">Our Story</p>
						<h2 class="story-block__title u-font-serif">Seit weit über 3 Jahrzehnten backen wir unsere Pizzen in unsere Pizzeria.</h2>
						<p class="u-color-grey">Viele Millionen verkaufte Pizzen, zahlreiche Bestbewertungen in diversen Medien, unser einzigartiges Mittagsbuffet und zehntausend zufriedene Stammgäste beweisen:

                                "Herzlich willkommen in den besten Pizzerien der Stadt !"
                                
                                Nur bei uns gibt es die Pizzacard. Mit dieser Pizzacard werden unseren Gäste zahlreiche Vergünstigungen in allen unserer Pizzerien geboten .
                                
                                Wir sehen uns als Familienpizzeria.Unsere Kindergeburtspartys sind einmalig in Österreich und die Ausstattung (z.B.Kinderhochstühle und Wickeltische) unserer Pizzerien mit zahlreichen Kinderspielräumen und Nichtraucherecken machen uns zur Familenpizzeria Nr.1 in Wien.</p>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!--=== end of Story ===-->

	<!--=== Slider: Popular items ===-->
	<div class="slider-section u-bg-secondary mt-7 mb-7">
		<div class="container">
			<div class="row">
				<div class="col-sm-12 text-center">
					<h2 class="u-h1 u-font-serif u-weight-700 mb-3">Most Popular</h2>
					<p class="mb-5">Customers most orderd meals | You know what is tasty and jammy</p>
				</div>
			</div>
			<div class="row js-slider">
				<div class="col-md-4 js-slide">
					<div class="slider-item" style="background-image: url('assets/img/home/margarita.jpg');">
						<span>Pizza Margarita</span>
					</div>
				</div>
				<div class="col-md-4 js-slide">
					<div class="slider-item" style="background-image: url('assets/img/home/daniele.jpg');">
						<span>Pizza Daniele</span>
					</div>
				</div>
				<div class="col-md-4 js-slide">
					<div class="slider-item" style="background-image: url('assets/img/home/tortellini.jpg');">
						<span>Tortellini Il mare</span>
					</div>
				</div>
				<div class="col-md-4 js-slide">
					<div class="slider-item" style="background-image: url('assets/img/home/spaghetti.jpg');">
						<span>Spaghetti Fabio's</span>
					</div>
				</div>
				<div class="col-md-4 js-slide">
					<div class="slider-item" style="background-image: url('assets/img/home/salmon.jpg');">
						<span>Salmon</span>
					</div>
				</div>
				<div class="col-md-4 js-slide">
					<div class="slider-item" style="background-image: url('assets/img/home/salat.jpg');">
						<span>Mixed Salad</span>
					</div>
				</div>
				<span class="slider-arrow slider-arrow--prev js-arrow-prev">
					<svg width="40" height="10" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 40 10"><path fill="none" stroke="#dcb36e" d="M5 10L1 5l4-5M40 5H1.5"/></svg>
				</span>
				<span class="slider-arrow slider-arrow--next js-arrow-next">
					<svg width="40" height="10" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 40 10"><path fill="none" stroke="#dcb36e" d="M36 0l4 5-4 5M1 5h38.5"/></svg>
				</span>
			</div>
		</div>
	</div>
	<!--=== end of Slider: Popular items ===-->

	<!--=== Menu ===-->
	<div class="menu mt-7 mb-8" id="menu">
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<h2 class="u-h1 u-font-serif u-weight-700 text-center mb-4">Menu</h2>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-12">
					<ul class="menu-nav d-flex justify-content-center mt-4 mb-5">
						<li class="mx-4 mb-3"><a href="#bread" class="menu-nav__link u-color-primary js-menu-link">Pizza</a></li>
						<li class="mx-4 mb-3"><a href="#pastry" class="menu-nav__link js-menu-link">Pasta</a></li>
						<li class="mx-4 mb-3"><a href="#coffee" class="menu-nav__link js-menu-link">Coffee & Drinks</a></li>
					</ul>
					<div class="menu-tab js-menu-tab" id="bread">
						<div class="d-md-flex">
							<ul class="menu-list mr-md-4">
								<li class="menu-list-item">
									<h4 class="menu-list-item__title u-font-serif">Provinciale<sup></sup></h4>
									<div class="d-flex">
										<span class="menu-list-item__desc font-italic">Tomaten / Mozzarella / Speck / Mais / Pfefferoni</span>
										<span class="menu-list-item__dots"></span>
										<span class="menu-list-item__price">€9.80</span>
									</div>
								</li>
								<li class="menu-list-item">
									<h4 class="menu-list-item__title u-font-serif">Di Carne<sup></sup></h4>
									<div class="d-flex">
										<span class="menu-list-item__desc font-italic">Tomatensauce / Butterkäse / Fleischragout</span>
										<span class="menu-list-item__dots"></span>
										<span class="menu-list-item__price">€10.20</span>
									</div>
								</li>
								<li class="menu-list-item">
									<h4 class="menu-list-item__title u-font-serif">Capricioza<sup></sup></h4>
									<div class="d-flex">
										<span class="menu-list-item__desc font-italic">Tomaten / Mozzarella / Salami / Champions / Sardellen</span>
										<span class="menu-list-item__dots"></span>
										<span class="menu-list-item__price">€12.00</span>
									</div>
								</li>
								<li class="menu-list-item">
									<h4 class="menu-list-item__title u-font-serif">Daniele<sup></sup></h4>
									<div class="d-flex">
										<span class="menu-list-item__desc font-italic">Prosciuto / Gorgonzola / Ruccola</span>
										<span class="menu-list-item__dots"></span>
										<span class="menu-list-item__price">€12.00</span>
									</div>
								</li>
							</ul>
							<ul class="menu-list ml-md-4">
								<li class="menu-list-item">
									<h4 class="menu-list-item__title u-font-serif">Palestra<sup>1,4</sup></h4>
									<div class="d-flex">
										<span class="menu-list-item__desc font-italic">LowCarb / Thunfisch / Oliven / Ruccola</span>
										<span class="menu-list-item__dots"></span>
										<span class="menu-list-item__price">€12.00</span>
									</div>
								</li>
								<li class="menu-list-item">
									<h4 class="menu-list-item__title u-font-serif">Vitale<sup>1,2</sup></h4>
									<div class="d-flex">
										<span class="menu-list-item__desc font-italic">LowCarb / Blattspinat / Schafkäse</span>
										<span class="menu-list-item__dots"></span>
										<span class="menu-list-item__price">€13.00</span>
									</div>
								</li>
								<li class="menu-list-item">
									<h4 class="menu-list-item__title u-font-serif">Vegetario<sup>1,2,3</sup></h4>
									<div class="d-flex">
										<span class="menu-list-item__desc font-italic">Tomaten / Veganer Käse / Paprika / Mais / Zwibel</span>
										<span class="menu-list-item__dots"></span>
										<span class="menu-list-item__price">€11.20</span>
									</div>
								</li>
								<li class="menu-list-item">
									<h4 class="menu-list-item__title u-font-serif">Asperago<sup>2,3</sup></h4>
									<div class="d-flex">
										<span class="menu-list-item__desc font-italic">Tomaten / Veganer Käse / Spargel / Mais </span>
										<span class="menu-list-item__dots"></span>
										<span class="menu-list-item__price">€10.80</span>
									</div>
								</li>
							</ul>
						</div>
					</div>
					<div class="menu-tab js-menu-tab" id="pastry">
						<div class="d-md-flex">
							<ul class="menu-list mr-md-4">
								<li class="menu-list-item">
									<h4 class="menu-list-item__title u-font-serif">Bolognese</h4>
									<div class="d-flex">
										<span class="menu-list-item__desc font-italic"> Spaghetti / Fleischragout / Parmesan</span>
										<span class="menu-list-item__dots"></span>
										<span class="menu-list-item__price">€8.80</span>
									</div>
								</li>
								<li class="menu-list-item">
									<h4 class="menu-list-item__title u-font-serif">Carbonara</h4>
									<div class="d-flex">
										<span class="menu-list-item__desc font-italic">Spaghetti / Obers und Egg / Schinken / Parmesan<sup></sup></span>
										<span class="menu-list-item__dots"></span>
										<span class="menu-list-item__price">€8.80</span>
									</div>
								</li>
								<li class="menu-list-item">
									<h4 class="menu-list-item__title u-font-serif">Aglio e Olio<sup>2,3</sup></h4>
									<div class="d-flex">
										<span class="menu-list-item__desc font-italic">Penne / Olivenöl  / Knoblauch </span>
										<span class="menu-list-item__dots"></span>
										<span class="menu-list-item__price">€7.80</span>
									</div>
								</li>
							</ul>
							<ul class="menu-list ml-md-4">
								<li class="menu-list-item">
									<h4 class="menu-list-item__title u-font-serif">Il Mare</h4>
									<div class="d-flex">
										<span class="menu-list-item__desc font-italic">Tagliatelle/ Garnelen / Pesto Genovese / Basilikum<sup>1,2</sup></span>
										<span class="menu-list-item__dots"></span>
										<span class="menu-list-item__price">€11.60</span>
									</div>
								</li>
								<li class="menu-list-item">
									<h4 class="menu-list-item__title u-font-serif">Fabio's</h4>
									<div class="d-flex">
										<span class="menu-list-item__desc font-italic">Tomatensauce / Knoblauch / Paprika / Oliven <sup>1,2,3</sup></span>
										<span class="menu-list-item__dots"></span>
										<span class="menu-list-item__price">€9.20</span>
									</div>
								</li>
								<li class="menu-list-item">
									<h4 class="menu-list-item__title u-font-serif">Figaro</h4>
									<div class="d-flex">
										<span class="menu-list-item__desc font-italic">Tortellini /Schafkäse / Basilikum <sup></sup> </span>
										<span class="menu-list-item__dots"></span>
										<span class="menu-list-item__price">€9.20</span>
									</div>
								</li>
							</ul>
						</div>
					</div>
					<div class="menu-tab js-menu-tab" id="coffee">
						<div class="d-md-flex">
							<ul class="menu-list mr-md-4">
								<li class="menu-list-item">
									<div class="d-flex">
										<h4 class="menu-list-item__title u-font-serif">Espresso</h4>
										<span class="menu-list-item__dots"></span>
										<span class="menu-list-item__price">€2.10</span>
									</div>
								</li>
								<li class="menu-list-item">
									<div class="d-flex">
										<h4 class="menu-list-item__title u-font-serif">Capuccino</h4>
										<span class="menu-list-item__dots"></span>
										<span class="menu-list-item__price">€3.40</span>
									</div>
								</li>
								<li class="menu-list-item">
									<div class="d-flex">
										<h4 class="menu-list-item__title u-font-serif">Latte</h4>
										<span class="menu-list-item__dots"></span>
										<span class="menu-list-item__price">€3.80</span>
									</div>
								</li>
							</ul>
							<ul class="menu-list ml-md-4">
								<li class="menu-list-item">
									<div class="d-flex">
										<h4 class="menu-list-item__title u-font-serif">Hot Chocolate</h4>
										<span class="menu-list-item__dots"></span>
										<span class="menu-list-item__price">€4.20</span>
									</div>
								</li>
								<li class="menu-list-item">
									<div class="d-flex">
										<h4 class="menu-list-item__title u-font-serif">Tea</h4>
										<span class="menu-list-item__dots"></span>
										<span class="menu-list-item__price">€3.10</span>
									</div>
								</li>
								<li class="menu-list-item">
									<div class="d-flex">
										<h4 class="menu-list-item__title u-font-serif">Lemonade</h4>
										<span class="menu-list-item__dots"></span>
										<span class="menu-list-item__price">€4.40</span>
									</div>
								</li>
							</ul>
						</div>
					</div>
					<div class="text-center u-text-small u-color-grey">
						<p>1 - Gluten Free &nbsp; &nbsp; 2 - Vegetarian &nbsp; &nbsp;  3 - Vegan &nbsp; &nbsp;  4 - Sugar free</p>
					</div>
					<div class="text-center mt-5">
						<a href="/menu" class="btn">See Full Menu</a>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!--=== end of Menu ===-->

	<!--=== Testimonials ===-->
	<div class="u-bg-cover u-bg-p-center" style="background-image: url('assets/img/home/pizzalow.jpg')">
		<div class="container">
			<div class="row">
				<div class="col-sm-10 offset-sm-1 pt-6">
					<h3 class="u-font-serif u-h1 u-weight-700 text-center">LOW CARB TEIG | VOLLCORN TEIG</h3>
					<div class="testimonials js-slider-1 text-center pt-5 pb-6">
						<div class="testimonial js-slide">
							<p class="testimonial__quote u-font-serif">"Unser Pizzateam hat monatelang geforscht,probiert, noch einmal geforscht, und noch viele Male probiert und verkostet"</p>
							<hr class="short-line short-line--black mt-4 mb-4">
						</div>
						<div class="testimonial js-slide">
							<p class="testimonial__quote u-font-serif">"Und jetzt präsentieren wir die 1. Low Carb Pizza aus dem Holzofen in Österreich!"</p>
							<hr class="short-line short-line--black mt-4 mb-4">
						</div>
						<div class="testimonial js-slide">
							<p class="testimonial__quote u-font-serif">"-52% Kohlenhydrade | +300% Eiweiss | *60.6 g Eiweiss *54.5g Kohlenhydrate *12.5g Fett"</p>
							<hr class="short-line short-line--black mt-4 mb-4">
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!--=== end of Testimonials ===-->

	<!--=== Featured Extras ===-->
	<div class="mt-7 mb-7">
		<div class="container">
			<div class="row">
				<div class="col-sm-12 text-center">
					<h2 class="u-h1 u-weight-700 u-font-serif mb-3">We offer also</h2>
					<p class="mb-5">Monday to Monday | We don't stop</p>
				</div>
			</div>
			<div class="row">
				<div class="col-md-4 mb-4">
					<a href="/buffet" class="article-card" data-aos="fade-up">
						<div class="article-card__img-wrapper">
							<div class="article-card__img" style="background-image:url('assets/img/home/buffet.jpg')"></div>
						</div>
						<h3 class="u-h5 u-font-serif mt-3">Mittagsbuffet</h3>
						<p class="article-card__date">11-14 Uhr</p>
					</a>
				</div>

				<div class="col-md-4 mb-4">
					<a href="/birthday" class="article-card" data-aos="fade-up" data-aos-delay="100">
						<div class="article-card__img-wrapper">
							<div class="article-card__img" style="background-image:url('assets/img/home/happy.jpg')"></div>
						</div>
						<h3 class="u-h5 u-font-serif mt-3">Kinderparty</h3>
						<p class="article-card__date">Just 15 Euro per Nose</p>
					</a>
				</div>

				<div class="col-md-4 mb-4">
					<a href="/catering" class="article-card" data-aos="fade-up" data-aos-delay="200">
						<div class="article-card__img-wrapper">
							<div class="article-card__img" style="background-image:url('assets/img/home/catering.jpg')"></div>
						</div>
						<h3 class="u-h5 u-font-serif mt-3">Catering</h3>
						<p class="article-card__date">For 10 to 150 People</p>
					</a>
				</div>
			</div>
		</div>
	</div>
	<!--=== end of Featured Extras ===-->

	<!--=== Footer Call to Action ===-->
	<div class="footer-cta" style="background-image:url('/assets/img/home/vegetables.jpg')">
		<div class="container">
			<div class="row">
				<div class="col-sm-10 offset-sm-1 text-center u-color-white">
					<h4 class="footer-cta__title u-font-serif" data-aos="fade-up">Like our products?</h4>
					<p class="footer-cta__subtitle" data-aos="fade-up" data-aos-delay="100">Order for yourself or send as gift for your loved ones.</p>
					<a href="/shopping" class="btn btn--big" data-aos="fade-up" data-aos-delay="100">Order Online</a>
				</div>
			</div>
		</div>
	</div>
	<!--=== end of Footer Call to Action ===-->


</body>
</html>


