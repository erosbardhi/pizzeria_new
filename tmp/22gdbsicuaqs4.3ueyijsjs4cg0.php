<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lobster">
<link rel="stylesheet" href="shop-test.css">

<body>


<main class="main-container">

        <section class="section">
            <h1 class="text-center">Products</h1>
            <div class="products">
              <?php foreach (($products?:[]) as $row): ?>
                <div class="product">
                    <img class="product__image" src="img/products/beer.svg" alt="Beer">
                    <h2 class="product__name"><?= ($row['product_name']) ?></h2>
                    <h3 class="product__price"><?= ($row['product_price']) ?></h3>
                    <i class="fas fa-info-circle"></i>
                    <i class="fas fa-plus"></i>
                    <button class="btn btn--primary" data-action="ADD_TO_CART">Add To Cart</button>
                </div>
            <?php endforeach; ?>  
            </div>
        </section>

        <section class="section">
            <h2 class="text-center">Cart</h2>
            <div class="cart"></div>
        </section>
    </main>

    <script src="/shop.js"></script>
</body> 