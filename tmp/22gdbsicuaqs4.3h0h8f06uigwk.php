<?php echo $this->render('/views/modules/alert.html',NULL,get_defined_vars(),0); ?>

<head>
  <link rel="stylesheet" href="/css/register.css" />
</head>

<div class="wrapper">
  <div class="title">
    Registration Form
  </div>

  <form action="/" method="post">
    <div class="inputfield">
      <label>First Name</label>
      <?php if ($errors['name']): ?>
        <div class="field-error"><?= ($errors['name']) ?></div>
      <?php endif; ?>
      <input type="text" class="input" name="name" id="name" value="<?= ($values['name']) ?>" />
    </div>
    <div class="inputfield">
      <label>Last Name</label>
      <?php if ($errors['last_name']): ?>
        <div class="field-error"><?= ($errors['last_name']) ?></div>
      <?php endif; ?>
      <input type="text" class="input" name="lastname" id="lastName" value="<?= ($values['last_name']) ?>" />
    </div>
    <div class="inputfield">
      <label>Password</label>
      <?php if ($errors['password']): ?>
        <div class="field-error"><?= ($errors['password']) ?></div>
      <?php endif; ?>
      <input type="password" class="input" name="password" id="Password" />
    </div>
    <div class="inputfield">
      <label>Email Address</label>
      <?php if ($errors['user_email']): ?>
        <div class="field-error"><?= ($errors['user_email']) ?></div>
      <?php endif; ?>
      <input type="text" class="input" name="email" id="Email" value="<?= ($values['user_email']) ?>" />
    </div>
    <div class="inputfield">
      <label>Phone Number</label>
      <?php if ($errors['user_phone']): ?>
        <div class="field-error"><?= ($errors['user_phone']) ?></div>
      <?php endif; ?>
      <input type="text" class="input" name="phonenumber" id="phoneNumber" value="<?= ($values['user_phone']) ?>" />
    </div>
    <div class="inputfield">
      <label>Address</label>
      <?php if ($errors['address']): ?>
        <div class="field-error"><?= ($errors['address']) ?></div>
      <?php endif; ?>
      <input type="text" class="input" name="address" id="adress" value="<?= ($values['address']) ?>" />
    </div>
    <br />
    <div class="inputfield">
      <input type="submit" value="Register" class="button is-success is-inverted is-outlined" />
    </div>
  </form>
</div>