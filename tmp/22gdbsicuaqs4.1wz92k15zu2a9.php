<!-- <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lobster"> -->
<link rel="stylesheet" href="css/shop.css">
<body>
<main class="main-container">
        <section class="section">
            <h1 class="text-center"><b>Products</b></h1>
            <div class="products">
              <?php foreach (($products?:[]) as $row): ?>
                <div class="product">
                   <img class="product__image" src="img/products/pizza-1.jpg" alt="NotFound"> 
                   <!--  <?php if ($errors['product_image']): ?>
                            <div class="field-error"><?= ($errors['product_image']) ?></div>
                        <?php endif; ?>
                        <img class="product__image"><?= ($row['product_image']) ?> -->
                    <?php if ($errors['product_name']): ?>
                        <div class="field-error"><?= ($errors['product_name']) ?></div>
                    <?php endif; ?>
                    <h6 class="product__name"><?= ($row['product_name']) ?></h6>
                    <?php if ($errors['product_description']): ?>
                        <div class="field-error"><?= ($errors['product_description']) ?></div>
                    <?php endif; ?>
                    <p class="product_description"><?= ($row['product_description']) ?></p>
                    <?php if ($errors['product_price']): ?>
                        <div class="field-error"><?= ($errors['product_price']) ?></div>
                    <?php endif; ?>
                    <h3 class="product__price"><?= ($row['product_price']) ?></h3>
                    <!-- <i class="fas fa-info-circle"></i>
                    <i class="fas fa-plus"></i> -->
                    <button class="btn btn--primary" data-action="ADD_TO_CART">Add To Cart</button>
                </div>
            <?php endforeach; ?>  
            </div>
        </section>
        <section class="section">
            <h2 class="text-center"><ion-icon name="basket"></ion-icon>Cart</h2>
            <div class="cart"></div>
        </section>
    </main>

    <script src="/js/shop.js"></script> 

</body> 