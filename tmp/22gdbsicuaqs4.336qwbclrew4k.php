<div class="demo">
        <table class="table is-responsive">
          <thead>
            <tr>
              <!-- <th> ID</th> -->
              <th> Password</th>
              <th> Name</th>
              <th> Surname</th>
              <th> Address</th>
              <th> Phone</th>
              <th> Email</th>
            </tr>
          </thead>
          <tbody>
            <!-- Das ist ein Foreach von Fatfree (repeat) -->
              <?php foreach (($users?:[]) as $row): ?>
                  <tr>
                    
                      <td><?= ($row['password']) ?></td>
                      <td><?= ($row['name']) ?></td>
                      <td><?= ($row['last_name']) ?></td>  
                      <td><?= ($row['address']) ?></td>
                      <td><?= ($row['user_phone']) ?></td> 
                      <td><?= ($row['user_email']) ?></td> 

                  <td><a href="/all-users/<?= ($row['user_id']) ?>/delete" class="button is-danger">Delete</a></td>
                  </tr>
      
              <?php endforeach; ?>  
          </tbody>
        </table>
      
      </div>