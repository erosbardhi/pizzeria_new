<div class="demo">
        <table class="table is-responsive">
          <thead>
            <tr>
              <!-- <th> ID</th> -->
              <th> Name</th>
              <th> Email</th>
              <th> Phone</th>
              <th> Date</th>
              <th> Time</th>
              <th> Nr.of Prs</th>
            </tr>
          </thead>
          <tbody>
            <!-- Das ist ein Foreach von Fatfree (repeat) -->
              <?php foreach (($reservation?:[]) as $row): ?>
                  <tr>
                      <!-- <td><?= ($row['reservation_id']) ?></td> -->
                      <td><?= ($row['res_name']) ?></td>
                      <!-- <td><?= ($row['res_message']) ?></td> -->
                      <td><?= ($row['res_email']) ?></td>
                      <td><?= ($row['res_phone']) ?></td>  
                      <td><?= ($row['res_date']) ?></td>
                      <td><?= ($row['res_time']) ?></td> 
                      <td><?= ($row['res_message']) ?></td> 
                  <td><a href="/table-all/<?= ($row['reservation_id']) ?>/delete" class="button is-danger">Delete</a></td>
                  </tr>
      
              <?php endforeach; ?>  
          </tbody>
        </table>
      
      </div>
      